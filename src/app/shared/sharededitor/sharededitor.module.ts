import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AladinComponent } from './aladin/aladin.component';

import { ColorCircleModule } from 'ngx-color/circle'; 
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AladinComponent,
    
  ],
  exports:[
    AladinComponent,
    

  ],
  imports: [
    CommonModule,
    ColorCircleModule,
    FormsModule
  ],
  
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class SharededitorModule { }

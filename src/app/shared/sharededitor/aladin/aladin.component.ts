import { Component, OnInit,Input } from '@angular/core';
import { HttpService, ListService,LocalService,AladinService,CartService } from 'src/app/core';
import { NeweditorService } from 'src/app/core';
import { fabric } from 'fabric';
declare  var require:any;
var myalert=require('sweetalert2');
import { ColorEvent } from 'ngx-color';
import { isNgTemplate } from '@angular/compiler';
declare var require :any
var $ = require("jquery")
@Component({
  selector: 'app-aladin',
  templateUrl: './aladin.component.html',
  styleUrls: ['./aladin.component.scss']
})
export class AladinComponent implements OnInit {

  constructor(private Editor:NeweditorService,private http:HttpService,private forms:ListService, private local:LocalService,private aladin:AladinService,private cartInfo: CartService) {}
  realPrice:any
  knowDay = new Date().getDay()
  pallette=false;
  produite=false;
  modeles=false
  formes=false
  product:any={}
  doubled=false
  colors=["blue","white","black","red","orange","green","#999999","#454545","#800080","#000080","#00FF00","#800000","brown","#2596be","#2596be","#be4d25"]
  current_page:any=1
  img:any
  model:any
  models:any=[]
  title = 'editor';
  file3:any
  viewimage:any
  canvas:any
  width=511;
  height=300;
  testingpicture:any;
  text:any;
  products:any=[]
  state:any=[];
  undo:any=[];
  redo:any=[];
  text40:any
  cpt=0;
  mods=0;
  textalign=["left","center","justify","right"]
  hastext=false
  cacheimg=true
  cacheimg1=true
  clipart:any
  produit=false
  texte=false
  forme=false
  modele=false
  @Input() data:any
  prourl2:any
  origin_1=null;
  origin_2=null;
  face1:any
  face2:any
  text_width=38
  newleft=0;
  isRedoing=false;
  isface1=true
  isface2=false
  cmpt:any=0
  cptr=0;
  cart_items=0;
  objectsState=[{f1:"",f2:""}]
  fonts = [
  {name:"Flowerheart",url:"./assets/fonts/FlowerheartpersonaluseRegular-AL9e2.otf"},
  {name:"HussarBold",url:"./assets/fonts/HussarBoldWebEdition-xq5O.otf"},
  {name:'Branda',url:"./assets/fonts/Branda-yolq.ttf"},
  {name:"MarginDemo",url:"./assets/fonts/MarginDemo-7B6ZE.otf"},
  {name:"Kahlil",url:"./assets/fonts/Kahlil-YzP9L.ttf"},
  {name:"FastHand",url:"./assets/fonts/FastHand-lgBMV.ttf"},
  {name:"BigfatScript",url:"./assets/fonts/BigfatScript-jE96G.ttf"},
  {name:"Atlane",url:"./assets/fonts/Atlane-PK3r7.otf"},
  {name:"HidayatullahDemo",url:"./assets/fonts/Bismillahscript-4ByyY.ttf"},
  {name:"Robus",url:"./assets/fonts/HidayatullahDemo-mLp39.ttf"},
  {name:"Backslash",url:"./assets/fonts/Robus-BWqOd.otf"},
  {name:"ChristmasStory",url:"./assets/fonts/ChristmasStory-3zXXy.ttf"},
];

  Uplade(event:any){ 
  this.file3 =event.target.files[0] 
  const reader = new FileReader();
  reader.onload = () => {
   this.viewimage= reader.result;
   this.cacheimg=false
   this.cacheimg1=true
  };
  reader.readAsDataURL(this.file3);
   
   }

uplod(event:any){
  this.Uplade(event)
 }


 ngOnInit():void{
this.cart_items=this.local.cart_items.length;
this.plus();

this.data.head=false
this.data.editor=true
  this.forms.getclipart(this.current_page).subscribe(res=>{
    this.clipart=res;
  this.clipart=this.clipart.cliparts;
},
er=>{console.log(er)})

  this.canvas= new fabric.Canvas('aladin',{
    hoverCursor: 'pointer',
    selection: true,
    selectionBorderColor:'blue',
    fireRightClick: true,
    preserveObjectStacking: true,
    stateful:true,
    stopContextMenu:false,
    
  });
  
  this.canvas.filterBackend=new fabric.WebglFilterBackend(); 
  this.canvas.setWidth(this.width);
  this.canvas.setHeight(this.height);
  this.canvas.on('object:modified',() =>{
   
    
  });

  this.canvas.on("object:added",()=>{

    if(!this.isRedoing){
      this.state = [];
    }
    this.isRedoing = false;
  })

  this.canvas.on("object:created",(e:any)=>{

  })
 this.canvas.on("mouse:move",(e:any)=>{
  // this.contextmenu(e)
 })

  this.canvas.on("mouse:dblclick",(e:any)=>{
  if(this.canvas.getActiveObject().isType('image')){
    this.aladin.triggerMouse(document.getElementById("importing"));
      this.doubled=true
    }
    
  })

  this.canvas.on("selection:updated",(e:any)=>{

  })

  setTimeout(()=>
  {
    this.canvas.loadFromJSON(JSON.parse(this.data.item.obj),()=>{
      this.canvas.setWidth(this.data.width)
      this.canvas.setHeight(this.data.height)
      this.face1=this.canvas.toDataURL();
      if(this.data.url2!=null){
        this.face2=this.data.url2;

      }
      this.canvas.requestRenderAll()

    })
  },20)
 
  this.http.get().subscribe(
    res=>{
      this.model=res
      for(let item of this.model){
      this.model[this.model.indexOf(item)].description = JSON.parse(item.description);
      if(this.aladin.IsJsonString(item.obj)){
        this.getCanvasUrl(JSON.parse(this.model[this.model.indexOf(item)].obj),item).then(async (res)=>{
            }).catch((err)=>{ 
              console.log(err)
            })
     
      }
     
      }

     }
  )

 }

 async getCanvasUrl(obj:any,item:any){
  let canvas= new fabric.Canvas(null,{
    hoverCursor: 'pointer',
    selection: true,
    selectionBorderColor:'blue',
    fireRightClick: true,
    preserveObjectStacking: true,
    stateful:true,
    stopContextMenu:false,
  });

 return await  canvas.loadFromJSON(obj,(ob:any)=>{
  canvas.setHeight(item.height)
  canvas.setWidth(item.width)
   var product={
    url:canvas.toDataURL(),
    price:item.description.price,
    promo:item.description.promo,
    type:item.description.type,
    name:item.description.name,
    owner:item.owner,
    item:item,
    category:item.category
  }
  console.log(item.description.type,this.data.item.category==product.category)
  if(item.description.type=="models" && this.data.item.category==product.category){
      this.models.push(product);
  }
  if(item.description.type=="model" && this.data.item.category==product.category){
    this.models.push(product);
}
  if(item.description.type=="produits" && this.data.item.category==product.category){
    this.products.push(product)
  }
  
 })
}

//url2
async getCanvasUrl2(item:any){
  let canvas= new fabric.Canvas(null,{
    hoverCursor: 'pointer',
    selection: true,
    selectionBorderColor:'blue',
    fireRightClick: true,
    preserveObjectStacking:true,
    stateful:true,
    stopContextMenu:false,
  });
  canvas.setHeight(item.height)
  canvas.setWidth(item.width)
  return await  canvas.loadFromJSON(JSON.parse(item.objf),(ob:any)=>{
  this.face2=canvas.toDataURL()
 })

}

loadCanvas(item:any){
  this.objectsState[0].f1=""
  this.objectsState[0].f2=""
  this.face2=""
  this.isface1=true;
  this.isface2=false;
  this.cptr=0;
  this.cmpt=0;
  var newdata= {}
  var data:any
  if(this.knowDay==5){
    var price= item.description.promo
    console.log(price)
    console.log(parseInt(price)*parseInt(this.data.qtys))

    Object.assign(newdata,{
      aladin:this.data.aladin,
      category:this.data.category,
      type:this.data.type,
      size_type:this.data.size_type,
      t:parseInt(price)*parseInt(this.data.qtys),
      qtys:this.data.qtys,
      name:this.data.name,
      size:this.data.size,
      price:this.data.price,
      data:this.data

    })
  }else{
    var price= item.description.price
    console.log(price)
    console.log(parseInt(price)*parseInt(this.data.qtys))

    Object.assign(newdata,{
      aladin:this.data.aladin,
      category:this.data.category,
      type:this.data.type,
      size_type:this.data.size_type,
      t:parseInt(price)*parseInt(this.data.qtys),
      qtys:this.data.qtys,
      name:this.data.name,
      size:this.data.size,
      price:this.data.price,
      data:this.data

    })
  }
  

    if(JSON.parse(item.objf)!=null){
      data={
        item:{objf:item.objf,obj:item.obj},
      } 

    }else{
      data={
        item:{objf:null,obj:item.obj},
      } 
    }
    Object.assign(data,newdata)
    this.data=data
    this.canvas.loadFromJSON(item.obj,()=>{
    this.canvas.setWidth(item.width)
    this.canvas.setHeight(item.height)
    this.face1=this.canvas.toDataURL()
    this.canvas.requestRenderAll()
      if(JSON.parse(item.objf)!=null){
          this.getCanvasUrl2(item)
        
      }  
  })

}
 makeItalic(){
  this.Editor.italic(this.canvas)
}

Redo(){
if(this.state.length>0){
  this.isRedoing = true;
 this.canvas.add(this.state.pop());
}

}

Undo(){
if(this.canvas._objects.length>0){
  this.state.push(this.canvas._objects.pop());
  this.canvas.renderAll();
 }

 
}

makeBold(){
  this.Editor.bold(this.canvas)   
}

underlineText(){
  this.Editor.underline(this.canvas)
}

sendBack(){
  this.Editor.sendBack(this.canvas)
}

sendForward(){
  this.Editor.sendForward(this.canvas)
}
overlineText(){
  this.Editor.overline(this.canvas)
}

addText(){
  if(!this.hastext){
    this.Editor.addText(this.canvas);
    this.hastext=true;
  }
}

duplicate(){
  this.copy();
  this.paste()
}

copy(){
  this.Editor.copy(this.canvas)
}

paste(){
  this.Editor.paste(this.canvas)
}


save(savehistory:Boolean) {
  if (savehistory === true) {
    let myjson = JSON.stringify(this.canvas.toJSON()); 
    this.state.push(myjson);
}  
}


textAlign(val:any){
  this.Editor.textAlign(this.canvas,val)

}

removeItem(){
  this.Editor.remove(this.canvas);
}

textfont(item:any){
  let data= item.target.value
  console.log(data.substr(0,data.indexOf("*")))
  this.Editor.textfont(Object.assign({},{name:data.substr(0,data.indexOf("*")),url:data.substr(data.indexOf("*")+1,data.length-1)}),this.canvas)
}

InputChange(Inputtext:any){
  if(this.canvas.getActiveObject()!=undefined && this.canvas.getActiveObject().text){
    if(this.cpt==0){
      this.text=this.canvas.getActiveObject().text+" "+ this.text
      this.cpt=this.cpt+1
      this.canvas.getActiveObject().text= this.text
      this.canvas.requestRenderAll();
    }else{
      this.canvas.getActiveObject().text= this.text
      this.canvas.requestRenderAll();
    }

  }else{  
    let text= new fabric.Textbox(this.text,{
      top:200,
      left:200,
      fill:"blue",
      fontSize:38,
      fontStyle:'normal',
      cornerStyle:'circle',
      selectable:true,
      borderScaleFactor:1,
      overline:false,
      lineHeight:1.5
    });
    this.canvas.add(text).setActiveObject(text);
    this.canvas.renderAll(text);
    this.canvas.requestRenderAll();
    this.canvas.centerObject(text);
  }


}

texteclor($event:ColorEvent){
this.Editor.textcolor($event.color.hex,this.canvas);
}


setItem(event:any){
this.Editor.setitem(event,this.canvas)
}


onFileUpload(event:any){
  let file = this.file3;
  this.cacheimg=true
  this.cacheimg1=false
  if(!this.Editor.handleChanges(file)){
  if(!this.doubled){
    const reader = new FileReader();
  reader.onload = () => {
    let url:any = reader.result;
    fabric.Image.fromURL(url,(oImg) =>{
    oImg.set({
        scaleX:0.5,
        scaleY:0.5,
        crossOrigin: "Anonymous",
  });
    this.canvas.add(oImg).setActiveObject(oImg);
    this.canvas.centerObject(oImg);
    this.canvas.renderAll(oImg)

  })
    };
    reader.readAsDataURL(file);

  }else{
    var width=this.canvas.getActiveObject().width
    var height=this.canvas.getActiveObject().height
    var left=this.canvas.getActiveObject().left
    var top=this.canvas.getActiveObject().top
    this.aladin.remove(this.canvas);
    //this.save(true)
    const reader = new FileReader();
    reader.onload = () => {
    let url:any = reader.result;
    fabric.Image.fromURL(url,(oImg) =>{
    oImg.set({
        scaleX:0.5,
        scaleY:0.5,
        crossOrigin: "Anonymous",
        left:left,
        top:top,
        height:height,
        width:width
  });
    this.canvas.insertAt(oImg,1,false).setActiveObject();
    this.canvas.centerObject(oImg);
    this.canvas.renderAll(oImg)
    this.canvas.requestRenderAll();
    this.sendForward()
    this.doubled=false;
  })
    };
    reader.readAsDataURL(file);
  }
   
  }

}

InputSize(){
  var canvasWrapper:any = document.getElementById('wrapper');
  canvasWrapper.style.width = this.width;
canvasWrapper.style.height = this.height;
this.canvas.setWidth(this.width);
this.canvas.setHeight(this.width);
}

textwidth(){
if(this.canvas.getActiveObject().text){
  console.log(this.canvas.getActiveObject())
  this.canvas.getActiveObject().set({height:this.canvas.getActiveObject().height+1})
  this.canvas.getActiveObject().set({width:this.canvas.getActiveObject().width+1})
  this.canvas.getActiveObject().set({fontSize:this.canvas.getActiveObject().fontSize+1})
  this.text_width= this.canvas.getActiveObject().fontSize
  this.canvas.renderAll()
}

}

minus(){
  if(this.canvas.getActiveObject().text){
    console.log(this.canvas.getActiveObject())
    this.canvas.getActiveObject().set({height:this.canvas.getActiveObject().height-1})
    this.canvas.getActiveObject().set({width:this.canvas.getActiveObject().width-1})
    this.canvas.getActiveObject().set({fontSize:this.canvas.getActiveObject().fontSize-1})
    this.text_width= this.canvas.getActiveObject().fontSize
    this.canvas.renderAll()
  }
  
}


Savemodal(){
 if(this.data.item.objf!=null){
  if(this.isface1){
    this.face1=this.canvas.toDataURL()
  }
  if(this.isface2){
    this.face2=this.canvas.toDataURL()
  }
 }else if(this.data.item.objf==null){
   this.face2=null;
   this.face1=this.canvas.toDataURL()
 }
 if(this.knowDay==5 && this.data.promo!= undefined){
   this.realPrice = this.data.promo
  }else{
    this.realPrice = this.data.price
  }
  
  let data:any={
          face1:this.face1,
          face2:this.face2,
          type_product:"editor",
          t:this.data.t,
          category:this.data.category,
          name:this.data.name,
          size:this.data.size,
          qty:this.data.qtys,
          type:this.data.type,
          size_type:this.data.size_type,
          price:this.realPrice,
          data:this.data
        };

        //this.local.add(data);
        this.local.adtocart(data) 
        myalert.fire({
          title:'<strong>produit ajouté</strong>',
          icon:'success',
          html:
            '<h6 style="color:blue">Felicitation</h6> ' +
            '<p style="color:green">Votre design a été ajouté dans le panier</p> ' +
            '<a href="/cart">Je consulte mon panier</a>' 
            ,
          showCloseButton: true,
          focusConfirm: false,
        
        })

    console.log(data)
}


contextmenu(event:any){
  let item =this.canvas.getActiveObject()
  if(item.type=="textbox" && item){
 
  }
  return false
}

showpallette(){
  this.pallette=true;
   this.produite=false;
   this.modeles=false
   this.formes=false
}

showforme(){
  this.pallette=false;
   this.produite=false;
   this.modeles=false
   this.formes=true
}
showmodeles(){
  this.pallette=false;
   this.produite=false;
   this.modeles=true
   this.formes=false
}
showproduite(){
  this.pallette=false;
   this.produite=true;
   this.modeles=false
   this.formes=false
}
saveface1(){
 if(this.isface1){
   //this.canvas.clear()
   this.face1=this.canvas.toDataURL()
   var obj=this.canvas.toJSON(['lockMovementX','lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY'])
   this.getCanvas(JSON.stringify(obj))
  
 }else{
  if(this.cptr==0){
    this.face2=this.canvas.toDataURL()
    this.objectsState[0].f2=this.canvas.toJSON(['lockMovementX','lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY'])
    this.isface2=false
    this.isface1=true
    var obj= this.data.item.objf
    this.getCanvas(JSON.stringify(obj)) 
    this.cptr=this.cptr+1
   }
   if(this.cptr>0){
    this.face2=this.canvas.toDataURL()
    this.objectsState[0].f2=this.canvas.toJSON(['lockMovementX','lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY'])
    this.isface2=false
    this.isface1=true
    this.getCanvas(JSON.stringify(this.objectsState[0].f1)) 
   }
 }
}

saveface2(){
  if(this.isface2){
    this.face2=this.canvas.toDataURL()
    var obj=this.canvas.toJSON(['lockMovementX','lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY'])
    this.getCanvas(JSON.stringify(obj))
   }else{
   if(this.cmpt==0){
    this.face1=this.canvas.toDataURL()
    this.objectsState[0].f1=this.canvas.toJSON(['lockMovementX','lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY'])
    this.isface1=false
    this.isface2=true
    var obj= this.data.item.objf
    if(JSON.parse(obj)!=null){
      obj=JSON.parse(obj)
      this.getCanvas(JSON.stringify(obj)) 
      this.cmpt=this.cmpt+1
    }
   
   }
   if(this.cmpt>0){
    this.face1=this.canvas.toDataURL()
    this.objectsState[0].f1=this.canvas.toJSON(['lockMovementX','lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY'])
    this.isface1=false
    this.isface2=true
    this.getCanvas(JSON.stringify(this.objectsState[0].f2)) 

   }

   }
}

 getCanvas(ob:any){
  this.canvas.loadFromJSON(JSON.parse(ob),(ob:any)=>{
 })
}

plus(){
  this.cartInfo.cartUpdated.subscribe(
    cart_length=>{
      if(cart_length>0){
        this.cart_items=cart_length
        console.log(cart_length)
      }else{
        console.log(cart_length)
        this.cart_items=cart_length
      }
    }
  )
}
}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-editor-header',
  templateUrl: './editor-header.component.html',
  styleUrls: ['./editor-header.component.scss']
})
export class EditorHeaderComponent implements OnInit {
  cart_items=0
  constructor() { }

  
  ngOnInit(): void {
    if(localStorage.getItem('cart')){
      let item :any=localStorage.getItem('cart');
      item=JSON.parse(item);
      this.cart_items=item.length;

    }
  }
}

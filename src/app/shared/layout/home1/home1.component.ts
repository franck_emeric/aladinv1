import { Component, OnInit } from '@angular/core';
import { ListService } from 'src/app/core';
import { fabric } from 'fabric';
import { Subscription, interval } from 'rxjs';
@Component({
  selector: 'app-home1',
  templateUrl: './home1.component.html',
  styleUrls: ['./home1.component.scss']
})
export class Home1Component implements OnInit {
  subscription:any= Subscription
  products:any={}
   tops:any;
   clothes:any
   gadget:any
   packs:any
   disps:any
   print:any
   details:any={}
   produit: any = [];
   packages:any=[]
   hide=false
   hidepack=false
   hidebody=true
   url='/editor/tops/'

   Jour = new Date().getDay()
   cacheBulle:Boolean=false
   hidepromo:Boolean=false
   hideprice:Boolean=true
   
   dateNow = new Date();
   dDay = new Date('Nov 24 2021 00:00:00');
   milliSecondsInASecond = 1000;
   hoursInADay = 24;
   minutesInAnHour = 60;
   SecondsInAMinute  = 60;
 
   timeDifference:any
   secondsToDday:any
   minutesToDday:any
   hoursToDday:any
   daysToDday:any
  constructor(private l : ListService) { }

  ngOnInit(): void {

    if(this.Jour==5){
      this.hideprice=false
      this.hidepromo=true
    }

    this.subscription = interval(1000).subscribe(x => { 
      this.getTimeDifference(); 
    });

    this.l.getclothpage().subscribe(
      res=>{
        this.clothes = res.data
        for (let item of this.clothes) {
          item.description = JSON.parse(item.description);
          if(JSON.parse(item.objf)!=null){
            this.getCanvasUrl2(JSON.parse(item.objf), item)
            .then(async (res) => {
             })
            .catch((err) => {
              console.log(err);
             });
          }
         this.getCanvasUrl(JSON.parse(item.obj), item)
           .then(async (res) => {
            })
           .catch((err) => {
             console.log(err);
            });
            
       }
      
      },
      err=>{
        console.log(err)
      }
    )
   this.l.getpackpage().subscribe(
     res=>{
       console.log(res)
       this.packs=res.data
       for(let item of this.packs){
        item.description = JSON.parse(item.description);
        if(this.IsJsonString(item.objf) && JSON.parse(item.objf)!=null){
          this.getCanvasUrl2(JSON.parse(item.objf),item).then(async (res)=>{
             
           }).catch((err)=>{ 
             console.log(err)
           })
         }
        if(this.IsJsonString(item.obj)){
        this.getCanvasUrl(JSON.parse(item.obj),item).then(async (res)=>{
       
        }).catch((err)=>{ 
          console.log(err)
        })
       }
       
       
          }
     }
   )

  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


  getTimeDifference () {
    if(this.Jour==5){
      this.timeDifference = this.dDay.getTime() - new  Date().getTime();
      this.allocateTimeUnits(this.timeDifference);
      this.cacheBulle=true
    }else{
      this.cacheBulle=false
    }

    
  }

  allocateTimeUnits (timeDifference:any) {
    this.secondsToDday = Math.floor((timeDifference) / (this.milliSecondsInASecond) % this.SecondsInAMinute);
    console.log(this.secondsToDday)
    this.minutesToDday = Math.floor((timeDifference) / (this.milliSecondsInASecond * this.minutesInAnHour) % this.SecondsInAMinute);
    this.hoursToDday = Math.floor((timeDifference) / (this.milliSecondsInASecond * this.minutesInAnHour * this.SecondsInAMinute) % this.hoursInADay);
    this.daysToDday = Math.floor((timeDifference) / (this.milliSecondsInASecond * this.minutesInAnHour * this.SecondsInAMinute * this.hoursInADay));
  }


  async getCanvasUrl(obj: any, item: any) {
    var product:any
    let canvas = new fabric.Canvas(null, {
      hoverCursor: 'pointer',
      selection: true,
      selectionBorderColor: 'blue',
      fireRightClick: true,
      preserveObjectStacking: true,
      stateful: true,
      stopContextMenu: false,
    });

    return await canvas.loadFromJSON(obj, (ob: any) => {
      canvas.setHeight(400);
      canvas.setWidth(400);
       if(JSON.parse(item.objf)!=null)
{
  product = {
    url: canvas.toDataURL(),
    url2:this.products.url2,
    price: item.description.price,
    promo: item.description.promo,
    size: item.description.size,
    type: item.description.type,
    name: item.description.name,
    owner: item.owner,
    made_with: item.description.made_with,
    item:item,
    width: item.width,
    height: item.height,
  };
  //console.log(product)
  if(item.category=="1"){
    this.produit.push(product);
  }
  if(item.category=="2"){
    this.packages.push(product);
    console.log(this.packages)
  } 
}else
if(
  JSON.parse(item.objf)==null
){
  product = {
    url: canvas.toDataURL(),
    url2:null,
    price: item.description.price,
    promo: item.description.promo,
    size: item.description.size,
    type: item.description.type,
    name: item.description.name,
    owner: item.owner,
    made_with: item.description.made_with,
    item:item,
    width: item.width,
    height: item.height,
  };
  //console.log(product)
  if(item.category=="1"){
    this.produit.push(product);
  }
  if(item.category=="2"){
    this.packages.push(product);
    console.log(this.packages)
  } 
}
        
      
    });
    
  }


  async getCanvasUrl2(objf: any, item: any) {

    let canvas = new fabric.Canvas(null, {
      hoverCursor: 'pointer',
      selection: true,
      selectionBorderColor: 'blue',
      fireRightClick: true,
      preserveObjectStacking: true,
      stateful: true,
      stopContextMenu: false,
    });

    return await canvas.loadFromJSON(objf,(ob: any) => {
      canvas.setHeight(400);
      canvas.setWidth(400);
      this.products = {
        url2:canvas.toDataURL(),
       
       
      };
      

        
      
    });
    
  }
  showDetails(data:any){
  
    Object.assign(this.details,{url:data.url,url2:data.url2,made_with:data.made_with,price:data.price,name:data.name,size:data.size,promo:data.promo, item:data.item, width:data.width, height:data.height})
    console.log(this.details)
    this.hide=true
    this.hidebody=false
    this.hidepack=false
   
  
  }
  showDetals(data:any){
  
    Object.assign(this.details,{url:data.url,url2:data.url2,made_with:data.made_with,price:data.price,name:data.name,size:data.size,promo:data.promo, item:data.item, width:data.width, height:data.height})
    console.log(this.details)
    this.hide=false
    this.hidebody=false
    this.hidepack=true
   
  
  }

  ChangeComponent(value:boolean){
    this.hidebody=value
    this.hide=false
    this.hidepack=false
    console.log(value)
    
  }
  IsJsonString(str:any) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
  }
}

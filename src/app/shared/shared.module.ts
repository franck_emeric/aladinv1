import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './layout/footer/footer.component';
import { HeaderComponent } from './layout/header/header.component';
import { Header3Component } from './layout/header3/header3.component';
import { Home1Component } from './layout/home1/home1.component';
import { HeaderUPComponent } from './layout/header-up/header-up.component';
import { FormsModule } from '@angular/forms';
import { FooterDashboardComponent } from './layout/footer-dashboard/footer-dashboard.component';
import { HeaderCategorieComponent } from './layout/header-categorie/header-categorie.component';
import { EditorHeaderComponent } from './layout/editor-header/editor-header.component';
import { CartitemsComponent } from './cartitems/cartitems.component';
import { SharededitorModule } from './sharededitor/sharededitor.module';
import { DescclothesComponent } from './layout/descclothes/descclothes.component';
import { DescpackagesComponent } from './layout/descpackages/descpackages.component';


@NgModule({
  declarations: [
    FooterComponent,
    HeaderComponent,
    Header3Component,
    Home1Component,
    HeaderUPComponent,
    FooterDashboardComponent,
    HeaderCategorieComponent,
    EditorHeaderComponent,
    CartitemsComponent,
    DescclothesComponent,
    DescpackagesComponent,
   
    
   

  ],
  imports: [
    CommonModule,
    FormsModule,
  ],
  exports:[
    FooterComponent,
    
    HeaderComponent,
    Header3Component,
    Home1Component,
    HeaderUPComponent,
    DescpackagesComponent,
    FormsModule,
    CommonModule,
    FooterDashboardComponent,
    HeaderCategorieComponent,
    EditorHeaderComponent,
    CartitemsComponent,
    SharededitorModule,

  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule { }
